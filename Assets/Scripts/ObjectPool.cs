﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ObjectPool : MonoBehaviour
{

    public static ObjectPool current;

    public GameObject bullet;

    private int aiBulletCount = 20;
    public List<GameObject> aiBullets = new List<GameObject>();

    private int playerBulletCount = 20;
    public List<GameObject> playerBullets = new List<GameObject>();


    void Awake()
    {
        current = this;
    }

    void Start()
    {
        initAiBulletPool();
        initPlayerBulletPool();

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("main2");        }
    }

    private void initAiBulletPool()
    {
        for (int i = 0; i < aiBulletCount; i++)
        {
            GameObject newBullet = (GameObject)Instantiate(bullet);
            newBullet.SetActive(false);
            aiBullets.Add(newBullet);
        }
    }

    private void initPlayerBulletPool()
    {
        for (int i = 0; i < playerBulletCount; i++)
        {
            GameObject newBullet = (GameObject)Instantiate(bullet);
            newBullet.SetActive(false);
            playerBullets.Add(newBullet);
        }
    }

    public GameObject GetPooledAiBulletObject()
    {
        GameObject targetObject = null;

        for (int i = 0; i < aiBullets.Count; i++)
        {
            if (!aiBullets[i].activeInHierarchy)
            {
                //Debug.Log("Object was not active.");
                return aiBullets[i];

            }
            else
            {

                //Debug.Log("Object was active.");
            }


        }

        return targetObject;
    }

    public GameObject GetPooledPlayerBulletObject()
    {
        GameObject targetObject = null;

        for (int i = 0; i < playerBullets.Count; i++)
        {
            if (!playerBullets[i].activeInHierarchy)
            {
                //Debug.Log("Object was not active.");
                return playerBullets[i];

            }
            else
            {

                //Debug.Log("Object was active.");
            }


        }

        return targetObject;
    }


}
