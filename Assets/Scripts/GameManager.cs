﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{


    //C# Property to get access to singleton instance
    //Read only - only has get accessor
    public static GameManager Instance
    {
        //return reference to private instance
        get
        {
            return instance;
        }

    }

    private static GameManager instance = null;

    //High score
    public int HighScore = 0;


    // Use this for initialization
    void Awake()
    {
        //Check if existing instance of class exists in scene
        //If so, then destroy this instance
        if (instance)
        {
            DestroyImmediate(gameObject);
            return;
        }

        //Make this active and only instance
        instance = this;

        //Make game manager persistent
        DontDestroyOnLoad(gameObject);
    }

    void Update()
    {
        Debug.Log(HighScore);
    }

}

