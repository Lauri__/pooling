﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {


    void OnEnable()
    {
        Invoke("SetInActive", 4.5f);
    }

    void SetInActive()
    {

        this.gameObject.SetActive(false);

    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
