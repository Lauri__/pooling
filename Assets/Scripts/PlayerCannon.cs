﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCannon : MonoBehaviour {

    public float rotateSpeed = 45;
    public float minRotation = -45;
    public float maxRotation = 45;

    private float startRotation;
    private float rotationOffset;
    private float coolDownTime;
    private float coolDownTimerMax = 0.15f;
    private float firePower = 250;

    public Transform spawnPosition;


    void Start()
    {
        startRotation = transform.rotation.eulerAngles.y;
        coolDownTime = 0f;

    }

    void Update()
    {
        //  Rotate using horizontal input.
        rotationOffset += Input.GetAxis("Horizontal") * rotateSpeed * Time.deltaTime;

        //  Cap rotation using min/max.
        if (rotationOffset > maxRotation)
            rotationOffset = maxRotation;
        if (rotationOffset < minRotation)
            rotationOffset = minRotation;

        //  Apply rotation.
        Vector3 setRotationAngles = new Vector3(transform.rotation.eulerAngles.x, startRotation + rotationOffset, transform.rotation.eulerAngles.z);
        Quaternion setRotation = Quaternion.identity;
        setRotation.eulerAngles = setRotationAngles;
        transform.rotation = setRotation;

        //Shoot
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (coolDownTime <= 0.0f)
            {
                coolDownTime = coolDownTimerMax;
                shoot();
            }
        }

        coolDownTime -= Time.deltaTime;
    }

    void shoot()
    {
        GameObject newBullet = ObjectPool.current.GetPooledPlayerBulletObject();

        if (newBullet == null)
        {
            //Debug.Log("Object was null.");
            return;
        }
        Rigidbody rb = newBullet.GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        newBullet.transform.position = spawnPosition.position;
        newBullet.transform.rotation = spawnPosition.rotation;
        newBullet.SetActive(true);
        rb.AddForce(spawnPosition.transform.forward * firePower, ForceMode.Impulse);

    }
}
