﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAi : MonoBehaviour
{

    public Transform target;
    public Transform sphereCastPoint;
    public Transform spawnPosition;
    BoxCollider box;
    public float rotateSpeed = 45;
    private float angle = 60;
    private float startRotation;
    private float coolDownTime;
    private float coolDownTimerMax = 0.30f;
    private float firePower = 250;


    void Start()
    {
        startRotation = transform.localEulerAngles.y;
        box = GetComponent<BoxCollider>();
        coolDownTime = 0f;
    }

    void Update()
    {

        Vector3 lookPos = target.position - transform.position;
        lookPos.y = 0;
        Quaternion rot = Quaternion.LookRotation(lookPos, transform.up);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, rotateSpeed * Time.deltaTime);
        //Debug.Log("Size: ");
        //Debug.Log(box.size.y);



        RaycastHit hit;

        if (Physics.Raycast(sphereCastPoint.transform.position, sphereCastPoint.transform.forward, out hit, 55f)){


            if (hit.collider.gameObject.name == "trg")
            {
                //Debug.Log("Success");
                Vector3 forward = sphereCastPoint.transform.TransformDirection(Vector3.forward) * 10;
                Debug.DrawRay(transform.position, forward, Color.green, .1f);
                shoot();
            }
        }
        coolDownTime -= Time.deltaTime;



    }

    private void shoot()
    {
        if (coolDownTime <= 0.0f)
        {
            coolDownTime = coolDownTimerMax;
            GameObject newBullet = ObjectPool.current.GetPooledAiBulletObject();

            if (newBullet == null)
            {
                //Debug.Log("Object was null.");
                return;
            }
            Rigidbody rb = newBullet.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            newBullet.transform.position = spawnPosition.position;
            newBullet.transform.rotation = spawnPosition.rotation;
            newBullet.SetActive(true);
            rb.AddForce(spawnPosition.transform.forward * firePower, ForceMode.Impulse);

            GameManager.Instance.HighScore++;
        }
    }

    void LateUpdate()
    {

        float y = transform.localEulerAngles.y;
        y = ClampAngle(y, startRotation - angle, startRotation + angle);
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, y, transform.localEulerAngles.z);

    }



    public float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {       // if angle in the critic region...
            if (angle > 180) angle -= 360;  // convert all angles to -180..+180
            if (max > 180) max -= 360;
            if (min > 180) min -= 360;
        }
        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0) angle += 360;  // if angle negative, convert to 0..360
        return angle;
    }


}